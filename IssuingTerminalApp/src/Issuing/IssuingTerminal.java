/**
 * ISSUING Terminal - HW Security Course
 *   @author Rafael Boix & Eduardo Novella
 */

package Issuing;

//Java SDK imports
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
//JCE imports & Javacard IO imports
import javax.smartcardio.*;
import javax.crypto.*;
import javax.crypto.spec.*;

import java.security.*;
// JCardSim imports -- based on bouncy castle -- for signature generation/verification a-la-JCOP
import javacard.framework.security.DESKey;
import javacard.framework.security.KeyBuilder;
import javacard.framework.security.Signature;

public class IssuingTerminal {
	Scanner keyboard = new Scanner(System.in);
	final private static double  VERSION  = 1.5;
	final private static String TERMINAL_ID="CK_TERM_RU0001IS";
	private boolean CARD_LEGIT;
	private CardChannel ch;
	private CardTerminal terminal;
	private DESKey sk;
	private Signature sg;
	private SecretKeySpec AES_KEY_CARD;
	private Cipher cp;
	private String cID; //For logging purposes
	//Logfile in /PATH_TO_cheapknip/logs/cheaplogIssuing.txt
	final private static String home = System.getProperty("user.dir")+"/../logs/cheaplogIssuing.txt";

	// APPLET_ID --> 10 Bytes
	final private static byte[] CHEAPKNIP_AID = { (byte) 0x63,(byte) 0x68,
		(byte) 0x65,(byte) 0x61,(byte) 0x70,(byte) 0x6B,(byte) 0x6E,
		(byte) 0x69,(byte) 0x70,(byte) 0x01 };
	// SELECT APDU 
	final private static CommandAPDU SELECT_APDU = new CommandAPDU(
			(byte) 0x00, (byte) 0xA4,(byte) 0x04, (byte) 0x00,CHEAPKNIP_AID );

	//APDU INS operation codes in the apdu header
	final static private byte INS_ISSUE_NEW_CARD = (byte)  0x10;
	final static private byte INS_HANDSHAKE      = (byte) 0x20;
	final static private byte INS_GET_TRNSCT_LOG = (byte) 0x60;
	final static private byte INS_GET_CARD_ID    = (byte)  0x69;
	final static short MAX_BALANCE       = (short) 0x61A8;
	//Log operation types
	final static private byte LOG_OP_PAY        = (byte)  0x11;
	final static private byte LOG_OP_TOPUP      = (byte)  0x22;
	final static private byte LOG_OP_ISSUE      = (byte)  0x33;
	// CONSTRUCTOR
	public IssuingTerminal(){
		TerminalFactory    tf;
		CardTerminals      ct;
		List<CardTerminal> cs;
		Card card;

		CARD_LEGIT=false;
		cID="0";
		sg = Signature.getInstance(Signature.ALG_DES_MAC8_ISO9797_M2, false);
		try {
			cp = Cipher.getInstance("AES/ECB/NoPadding");
		} catch (Exception e) {
			Utilities.writeToLogs(home,"[E] "+e.toString());			
		}
		try {
			tf = TerminalFactory.getDefault();
			ct = tf.terminals();
			cs = ct.list(CardTerminals.State.CARD_PRESENT);
			if (cs.isEmpty()){
				Utilities.writeToLogs(home,"[E] No terminal with a card found!");
				System.out.println("[-] No terminal with a card found!");
				return;
			}
			for(CardTerminal c : cs){
				if (c.isCardPresent()){
					try{
						card = c.connect("*");
						try{
							ch =card.getBasicChannel();
							ResponseAPDU resp = ch.transmit(SELECT_APDU);
							terminal=c;

							if (resp.getSW() !=  0x9000){
								Utilities.writeToLogs(home,"[E] Problems: Card Device not selectable != 0x9000");
								throw new Exception("[!] Problems: Card Device not selectable");
							}

						}catch(Exception e){
							Utilities.writeToLogs(home,"[E] "+e.toString());
							System.out.println("[-] Card isn't ok!");
						}
						//card.disconnect(false);
					}catch (CardException ce){
						Utilities.writeToLogs(home,"[E] "+ce.toString());
						System.out.println("[-] Couldn't connect to card!");
					}
					return;
				} else{
					Utilities.writeToLogs(home,"[W] No card present!");
					System.out.println("[-] No card present!");
				}
			} 
		} catch (CardException ce){
			Utilities.writeToLogs(home,"[E] "+ce.toString());
			System.out.println("[-] Card status problem!");
		}	
	} 


	public static  void printMainMenuOptions() {
		System.out.println("##################################################################################################");
		System.out.println("#                              Welcome to CHEAPKNIP terminal!                                    #");
		System.out.println("#                              ------------------------------                                    #");
		System.out.println("#                              1.- Issue a new card                                              #");
		System.out.println("#                              2.- Check card transaction log                                    #");
		System.out.println("#                              0.- Exit                                                          #");
		System.out.println("#                                                                                                #");
		System.out.println("#                                                                                                #");
		System.out.printf ("#                            2012 Cheapknip Payment  %.2f                                        #\n",VERSION);     
		System.out.println("##################################################################################################");
	}
	public  static void main(String[] arg) {
		System.out.printf("Terminal:%s\t\t\t\t\t\t\t%s\n",TERMINAL_ID,Utilities.getSystemDate());
		int opt = -1;
		IssuingTerminal it=new IssuingTerminal();

		do{	
			printMainMenuOptions();

			opt = Utilities.enterOption(0,2);
			try{
				switch (opt){
				case 1: 
					System.out.println("[1]Issue new card");
					/**
					 *  User details: name, address ,birthday  100 Bytes
					 *  Name     == 40 bytes
					 *  Address  == 54 bytes
					 *  Birthday == 6 bytes
					 */
					byte[] userInfo=new byte[100];
					System.out.print("[+] Enter user name (truncated to 40 chars max): ");
					System.arraycopy(Utilities.enterString(40), 0, userInfo, 0, 40);
					System.out.print("[+] Enter user address (truncated to 54 chars max): ");
					System.arraycopy(Utilities.enterString(54), 0, userInfo,40, 54);
					System.out.print("[+] Enter user birthdate in format DDMMYY: ");
					System.arraycopy(Utilities.enterString(6), 0, userInfo, 94, 6);
					boolean issuedOK=it.issueCard(userInfo);
					if(issuedOK){
						System.out.println("[+] SUCCESS - Card issued ok");
						Utilities.writeToLogs(home,"[+] Card issued ok -- Card: " + it.cID + "\n**Data:\n"+new String(userInfo)+"\n**End of Data");
						it.registerCardInDatabase(it.cID,userInfo);
					}
					else{
						System.out.println("[!] FAIL - Card issuing failed. Check message log.");
						Utilities.writeToLogs(home,"FAIL - Card issuing failed. Card: " + it.cID);
					}
					break;
				case 2:
					it.getTransactionLog();
					break;
				}	
			} catch(Exception e){
				e.printStackTrace();
				Utilities.writeToLogs(home,"Failure during card issuance:" + e);
			}

		}while(opt != 0);
		System.out.println("Thank you for using our CheapKnip Issuing Terminal!");System.exit(0);
	}

	private void registerCardInDatabase(String cID2, byte[] userInfo) {
		// Mockup function for registering the card id & issuance data in a DB	
	}


	/** 
	 * Issues a CheapKnip card with provided data. Card keys & data is initialized
	 * @return issuing status
	 * @throws CardException
	 * @author Eduardo Novella & Rafael Boix
	 */
	private boolean issueCard(byte[] userInfo) throws CardException{
		byte[] ck=null;
		byte[] sgn=null;
		//First, we get the cardID to compute key K (INS=0x69)
		CommandAPDU cardIDrequest = new CommandAPDU((byte) 0x00, INS_GET_CARD_ID,(byte) 0x00, (byte) 0x00,2);
		if(ch==null) throw new CardException("Card not present!");
		ResponseAPDU res= ch.transmit(cardIDrequest);
		byte[] cardID= res.getData();
		cID=Integer.toString((int)Utilities.getShort(cardID, 0));
		if (res.getSW() !=  0x9000){
			System.out.println("[!] Error reading card ID. Check card connection & reader. "); throw new CardException("Error while reading Card ID");
		}
		else{
			//We set the card AES key (for crypto) as AES(CardID)master_key
			//We set the card 3DES key (for signing) as AES(function(CardID))master_key

			//Card Key computation
			SecretKeySpec sks = new SecretKeySpec(getMasterKey(), "AES");
			try {
				Cipher cipher = Cipher.getInstance("AES");
				cipher.init(Cipher.ENCRYPT_MODE, sks);
				ck = cipher.doFinal(cardID); //AES key = 128 bit
				int len=cardID.length;
				byte[] sigCardID = Arrays.copyOf(cardID, 8*len);
				for(int i=len;i<8*len;i++)
					sigCardID[i]=(byte)(sigCardID[i-1]^sigCardID[i-2]*i);
				cipher.init(Cipher.ENCRYPT_MODE, sks);
				sgn = Arrays.copyOf(cipher.doFinal(sigCardID),24); //3DES key for signature purposes= 192 bit
			} catch (Exception e) {
				e.printStackTrace();
				Utilities.writeToLogs(home,"[E] CRITICAL: Error during issuing operation, failed AES & 3DES key generation while creating card "+cID);
			}

			//We have the keys, let's install the data in the card
			byte[] issueData=new byte[(16+24+100)];//AES key + 3DES key + User info
			System.arraycopy(ck, 0, issueData, 0, 16);
			System.arraycopy(sgn, 0, issueData, 16, 24);
			System.arraycopy(userInfo, 0, issueData, 40, 100);

			CommandAPDU cardIssue = new CommandAPDU((byte) 0x00, INS_ISSUE_NEW_CARD,(byte) 0x00, (byte) 0x00,issueData,110);
			res= ch.transmit(cardIssue);
			if (res.getSW() ==  0x6D00){
				System.out.println("[!] Error writing card information. Check card status (already issued?). ");
				Utilities.writeToLogs(home,"[E] Error writing card information. Check card status (already issued?). Card: "+cID);
				return false;
			}
			else if (res.getSW() !=  0x9000){
				System.out.println("[!] Error issuing card. Check card connection & reader and retry. ");
				throw new CardException("Error while checking issuing card info" + cID);
			}
			else{
				//1st verification step: we verify the signature
				initSignatureKey(cardID);
				if(verifySignature(res.getData(), 102)){
					System.out.println("[+] Card signature test passed OK");
					byte[] verifArray=new byte[102];
					byte[] decipheredArray = null;
					System.arraycopy(cardID, 0, verifArray, 0, 2);
					System.arraycopy(userInfo,0,verifArray,2,100);
					decipheredArray=Arrays.copyOf(res.getData(),102);
					//2nd&3rd verification steps: decrypt & check that decrypted data matches given data
					byte [] dec16FirstBytes=decryptAES128(decipheredArray, 0, 16);
					System.arraycopy(dec16FirstBytes,0,decipheredArray,0,16);
					if(Arrays.equals(verifArray, decipheredArray)){
						System.out.println("[+] Card crypto test passed OK. ");
						try{
							handshakeProtocol();
							System.out.println("[+] Handshake protocol test passed OK. ");
						}
						catch(CardException ce){
							Utilities.writeToLogs(home,"[E] Handshake protocol test FAILED. Destroy the card "+cID);
							System.out.println("[!] Handshake protocol test FAILED. Destroy the card! ");
							return false;
						}
						System.out.println("[+] Data written to the card OK. ");
						return true;
					}
					else{
						Utilities.writeToLogs(home,"[E] Issued data does not match card data. Destroy the card "+cID);
						System.out.println("[!] Issued data does not match card data. Destroy the card!");
						return false;
					}
				}
				else{
					Utilities.writeToLogs(home,"[E] Signature test FAILED. Destroy the card "+cID);
					System.out.println("[!] Card signature test FAILED. Destroy the card!");
					return false;
				}
			}
		}
	}

	private byte[] getMasterKey(){ //Mockup for a real implementation of getting the Master key
		//AES-128-key=16 bytes
		final String mk="1234567890123456"; //REMARK: THIS IS NOT A SAFE KEY; REMEMBER THAT THIS IS A MOCKUP FUNCTION
		return mk.getBytes();
	}

	private void handshakeProtocol() throws CardException {
		SecureRandom sr = new SecureRandom();
		//STEP 1: generate nonceT, send it to the Card as challenge in plaintext
		//----------------------------------------------------------
		byte[] nonceT = new byte[8];
		byte[] nonceT2 = new byte[8];
		sr.nextBytes(nonceT);
		sr.nextBytes(nonceT2);
		//Card handshake INS=0x20 ; response length=2bytes cardID+16bytes nonceR+8bytes signature
		CommandAPDU challenge1 = new CommandAPDU((byte) 0x00, (byte) 0x20,(byte) 0x00, (byte) 0x00,nonceT,18+8); 
		if(ch==null) throw new CardException("Card not present!");
		ResponseAPDU res= ch.transmit(challenge1);
		byte[] buff= res.getData();
		if (res.getSW() !=  0x9000){
			System.out.println("[!] Error reading the card. Aborting operation. "); throw new CardException("Error while handshake - step 1");
		}
		else{
			//Step 2: recover data from response APDU = cardID, nonceR
			//----------------------------------------------------------

			// CardID goes in plaintext
			byte[] cardID=Arrays.copyOfRange(buff, 16, 18);
			cID=Integer.toString((int)Utilities.getShort(cardID, 0)); //We put the read CardID in the cID variable for logging purposes

			// Compute & init card keys
			if (AES_KEY_CARD==null)
				AES_KEY_CARD=getKeyFromCardID();
			if (sk==null)
				initSignatureKey(cardID);
			if(!verifySignature(buff, 18)){
				System.out.println("[!] Error: check that your card is a CheapKnip card");
				Utilities.writeToLogs(home,"[E] Failed signature during handshake protocol test, step1. Card "+cID);
				CARD_LEGIT=false;
				return;
			}

			// Decrypt nonceR --> AES(nonceR)K
			byte[] cryptoBuff = decryptAES128(buff,0,16);
			byte[] nonceR    = Arrays.copyOf(cryptoBuff,8);

			byte[] nonceC_T2=new byte[16];
			//Recover nonceC from Card: nonceR XOR nonceT
			//We put nonceC in the first 8 bytes of the array
			for(int i=0;i<8;i++){
				nonceC_T2[i]=(byte)((nonceR[i])^(nonceT[i]));
			}
			for(int i=8;i<16;i++){
				nonceC_T2[i]=nonceT2[i-8];
			}
			//encrypt properly nonceC_T2

			//Step3: we send AES(nonceC,nonceT2)K back to the card and wait for response AES(nonceR2)K
			//----------------------------------------------------------
			byte[] enc_nonceC_T2=encryptAES128(nonceC_T2, 0, 16);
			//Note in step3: state is a parameter; P1 is 0x10!!
			CommandAPDU challenge2 = new CommandAPDU((byte) 0x00, INS_HANDSHAKE,(byte) 0x10, (byte) 0x00,enc_nonceC_T2,16+8);
			res= ch.transmit(challenge2);
			buff= res.getData();
			if(!verifySignature(buff, 16)){
				System.out.println("[!] Card error. Remove your card & try again");
				Utilities.writeToLogs(home,"[E] Failed signature during handshake protocol test, step2. Card "+cID);
				CARD_LEGIT=false;
				return;
			}
			if (res.getSW() !=  0x9000){
				System.out.println("[!] Error reading the card. Aborting operation. "); throw new CardException("Error while handshake - step 3");
			}
			else{
				//Step 4: check if card is legit: nonceT2 should be the same than the one in memory
				//----------------------------------------------------------
				cryptoBuff=Arrays.copyOf(buff, 16);
				buff=decryptAES128(cryptoBuff, 0, 16);
				byte[] nonceR2=Arrays.copyOf(buff,8); 

				//We rebuild nonceT2 from nonceR2 by doing XOR(nonceR2,nonceT)
				Utilities.XOR(nonceR2, nonceT, nonceR2, 0, 0, 0, 8);
				if(Arrays.equals(Arrays.copyOf(nonceR2, 8), nonceT2)){
					CARD_LEGIT=true;
				}
				else{
					CARD_LEGIT=false;
					System.out.println("[!] Error reading the card. Aborting operation. ");
					Utilities.writeToLogs(home,"[E] Bogus answer from card, failed challenge2. Card "+cID);
					System.exit(-1);
				}
			}
		}
	}

	/** 
	 * 
	 * CRYPTO FUNCTIONS 
	 * 
	 */

	/** 
	 * Sign message with MAC using same algorithm as the JavaCard does
	 * @author Eduardo Novella & Rafael Boix
	 */
	@SuppressWarnings("unused")
	private void signMessage(byte[] msg, int msgLength, byte[] signature, int signOffset){
		if(sk!=null)
		{sg.init(sk, Signature.MODE_SIGN);
		sg.sign(msg, (short)0, (short)msgLength, signature, (short)signOffset);
		}		
		else
			Utilities.writeToLogs(home,"[E] Signature key not initialized");
	}

	/** 
	 * Verify 8byte signature byte array
	 * @author Eduardo Novella & Rafael Boix
	 */
	private boolean verifySignature(byte[] msgPlusSignature, int msgLength){
		if(sk!=null)
		{sg.init(sk, Signature.MODE_VERIFY);
		return sg.verify(msgPlusSignature, (short)0, (short)msgLength, msgPlusSignature,(short)msgLength,(short)8);}
		else{
			Utilities.writeToLogs(home,"[E] Signature key not initialized"); return false;
		}
	}

	/** 
	 * Encrypt byte array
	 * @author Eduardo Novella & Rafael Boix
	 */
	public byte[] encryptAES128 (byte[] plain,int offset,int len) {
		byte[] encrypted=null;
		byte[] plainTrimmed = new byte[len];
		System.arraycopy(plain, offset, plainTrimmed, 0, len);
		try {
			if(AES_KEY_CARD==null)
				cp.init(Cipher.ENCRYPT_MODE,getKeyFromCardID());
			else
				cp.init(Cipher.ENCRYPT_MODE,AES_KEY_CARD);
			encrypted = cp.doFinal(plainTrimmed);
		} catch (Exception e){
			Utilities.writeToLogs(home,"[E] "+e.toString());
		}
		return encrypted;
	}

	/** 
	 * Decrypt byte array
	 * @author Eduardo Novella & Rafael Boix
	 */
	public byte[] decryptAES128 (byte[] encrypted,int offset,int len){
		byte[] plaintext=null;
		byte[] encryptedTrimmed = new byte[len];
		System.arraycopy(encrypted, offset, encryptedTrimmed, 0, len);
		try {
			if(AES_KEY_CARD==null)
				cp.init(Cipher.DECRYPT_MODE,getKeyFromCardID());
			else
				cp.init(Cipher.DECRYPT_MODE,AES_KEY_CARD);
			plaintext = cp.doFinal(encryptedTrimmed);
		} catch (Exception e){
			Utilities.writeToLogs(home,"[E] "+e.toString());
		}
		return plaintext;
	}

	/** 
	 * Get card key from card directly
	 * @author Eduardo Novella & Rafael Boix
	 */
	private SecretKeySpec getKeyFromCardID() throws CardException{
		SecretKeySpec k = null;
		CommandAPDU cardIDrequest = new CommandAPDU((byte) 0x00, (byte) 0x69,(byte) 0x00, (byte) 0x00,2);
		ResponseAPDU res= ch.transmit(cardIDrequest);
		byte[] cardID= res.getData();
		if (res.getSW() !=  0x9000){
			Utilities.writeToLogs(home,"[E] Error reading card ID. Check card connection & reader. ");
			throw new CardException("Error while reading Card ID");
		}
		else{
			//We set K as the CardID encrypted with the master key
			SecretKeySpec sks = new SecretKeySpec(getMasterKey(), "AES");
			try {
				Cipher cipher = Cipher.getInstance("AES");
				cipher.init(Cipher.ENCRYPT_MODE, sks);
				k= new SecretKeySpec(cipher.doFinal(cardID),"AES");

			} catch (Exception e) {
				Utilities.writeToLogs(home,"[E] "+e.toString());
			}
		}
		return k;

	}

	/** 
	 * Get card key from byte array
	 * @author Eduardo Novella & Rafael Boix
	 */
	@SuppressWarnings("unused")
	private SecretKeySpec getKeyFromCardID(byte[] cardID) throws CardException{
		SecretKeySpec k = null;
		SecretKeySpec sks = new SecretKeySpec(getMasterKey(), "AES");
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, sks);
			k= new SecretKeySpec(cipher.doFinal(cardID),"AES");

		} catch (Exception e) {
			Utilities.writeToLogs(home,"[E] "+e.toString());
		}
		return k;
	}

	/** 
	 * @author Eduardo Novella & Rafael Boix
	 */
	private boolean initSignatureKey(byte[] cardID){
		//Note that this works *in the terminal* due to the JCardSim wrapper of BouncyCastle :)
		SecretKeySpec sks = new SecretKeySpec(getMasterKey(), "AES");
		try {
			Cipher cipher = Cipher.getInstance("AES");
			int len=cardID.length;
			byte[] sigCardID = Arrays.copyOf(cardID, 8*len);
			for(int i=len;i<8*len;i++)
				sigCardID[i]=(byte)(sigCardID[i-1]^sigCardID[i-2]*i);
			cipher.init(Cipher.ENCRYPT_MODE, sks);
			byte[] sgn = Arrays.copyOf(cipher.doFinal(sigCardID),24); //3DES key for signature purposes= 192 bit
			sk = (DESKey) KeyBuilder.buildKey(KeyBuilder.TYPE_DES, KeyBuilder.LENGTH_DES3_3KEY, false);
			sk.setKey(sgn, (short)0);
		} catch (Exception e) {
			Utilities.writeToLogs(home,"[E] "+e.toString());
			return false;
		}
		return true;
	}

	/** 
	 * @author Eduardo Novella & Rafael Boix
	 */
	private String logOperationToString(byte logOp){
		switch(logOp){
		case LOG_OP_ISSUE:
			return new String("CARD_ISSUE");
		case LOG_OP_PAY:
			return new String("PAYMENT");
		case LOG_OP_TOPUP:
			return new String("TOPUP");
		}
		return new String("UNDEFINED");
	}

	private void getTransactionLog() throws CardException{
		if(!CARD_LEGIT) {
			try{
				handshakeProtocol();
			}
			catch(Exception e){
				System.out.println("Error talking with the card. Check if card is still not issued.");
				Utilities.writeToLogs(home,"[E] Error talking with the card. Check if card is still not issued. "+cID);
			}
		}
		if(CARD_LEGIT){
			if(terminal==null ||(terminal!=null && terminal.isCardPresent()==false)){
				Utilities.writeToLogs(home, "[E] Card was teared! Card ID:"+cID);
				System.out.println("Card was teared. Terminal will now reboot.");
				System.exit(-1);
			}
			CommandAPDU cardIDrequest = new CommandAPDU((byte) 0x00, INS_GET_TRNSCT_LOG,(byte) 0x00, (byte) 0x00,60);
			ResponseAPDU res= ch.transmit(cardIDrequest);
			byte[] trLog= res.getData();
			if (res.getSW() !=  0x9000){
				Utilities.writeToLogs(home,"[E] Error while reading transaction log. Card:"+cID);
				throw new CardException("Error while reading transaction log.");
			}
			else{
				if(!verifySignature(trLog, 52))
					Utilities.writeToLogs(home,"[E] LOG SIGNATURE FAILED - CHECK FOR TAMPERED DATA. Card:"+cID);
				int count=Utilities.getShort(trLog, 0);
				byte op;
				double oldbalance=0;
				double newbalance=0;
				int index;
				if(count<=10){
					System.out.println("Log of last operations ** "+Utilities.getSystemDate());
					System.out.println("#OP");
					for(int i=0;i<count;i++){
						op=trLog[(i*5)+2];
						oldbalance=(double)(Utilities.getShort(trLog, ((i*5)+3))/100.0);
						newbalance=(double)(Utilities.getShort(trLog, ((i*5)+5))/100.0);
						System.out.println((i+1)+".- Operation: "+logOperationToString(op)+" ** Old Balance: "+oldbalance+" ** New Balance: "+newbalance);
					}
				}
				else if (count>10){
					System.out.println("Log of last operations ** "+Utilities.getSystemDate());
					int correctIndexes=count%10; //Up to correctIndexes the indexes are in the same decade as count
					for(int i=0;i<10;i++){
						if(i<correctIndexes)
							index=count-correctIndexes+i;
						else
							index=count-correctIndexes-10+i;
						op=trLog[(i*5)+2];
						oldbalance=(double)(Utilities.getShort(trLog, ((i*5)+3))/100.0);
						newbalance=(double)(Utilities.getShort(trLog, ((i*5)+5))/100.0);
						System.out.println((index+1)+".- Operation: "+logOperationToString(op)+" ** Old Balance: "+oldbalance+" ** New Balance: "+newbalance);
					}
				}
			}
		}
	}
}

