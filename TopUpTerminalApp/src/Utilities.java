//Version 1.01

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Utilities {
	private  static Scanner keyboard = new Scanner(System.in);
	
	public static String getSystemDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);	
	}

	public static Scanner getKeyboard() {
		return keyboard;
	}

	public static String hex(int i){
		return Integer.toHexString(i).toUpperCase();
	}

	public static String APDUtoString(byte[] buffer) {
		StringBuffer sb = new StringBuffer();
		for(byte b : buffer){
			sb.append(String.format("%02X", b)+" ");
		}
		return sb.toString();
	}

	public static double enterDouble(){
		double amount=0;
		try{
			amount=keyboard.nextDouble();
		}
		catch (InputMismatchException e) { 
			System.err.println("[-] Input Error! Don't try funny things with the keyboard! :) ");
			keyboard.nextLine();}	
		catch (Exception e){
			System.err.println("[-]  Error! :(");
		}

		return Math.abs(amount);
	}
	public static int enterOption(int min,int max) {
		int opt = -1;
		do{
		System.out.print("[+] Enter an option:");

		try {
			opt= (int) keyboard.nextInt();
		}
		catch (InputMismatchException e) { 
			System.err.println("[-] Input Error! Don't try funny things with the keyboard! :) ");
			keyboard.nextLine();}	
		catch (Exception e){
			System.err.println("[-]  Error! :(");
			keyboard.nextLine();}
		}while(opt<min || opt>max);	
		
		return opt;
	}
	public static byte[] enterString(int length){
		byte[] str=null;
		try{
			String strTemp=null;
			InputStreamReader converter = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(converter);
			strTemp=in.readLine();
			str=Arrays.copyOf(strTemp.getBytes(), length);
		}
		catch (InputMismatchException e) { 
			System.err.println("[-] Input Error! Don't try funny things with the keyboard! :) ");
		}	
		catch (Exception e){
			System.err.println("[-]  Error! :(");
		}

		return (str);
	}

	
	public static short getShort(byte[] data, int offset) {       
	        return (short) (((data[0+offset] & 0x000000FF) << 8) | (data[1+offset] & 0x000000FF));
	}
	
	public static byte[] getArrayBytes(short srt){
		
		return new byte[]{(byte)((srt >> 8) & 0x000000FF),(byte)(srt & 0x000000FF)};
	}
	
	public static void XOR(byte[] buffer1, byte[] buffer2,byte[] bufferDest, int offset1, int offset2, int offsetDest, int length){
		int n=0;
		while(n<length){
			bufferDest[offsetDest+n] = (byte) (buffer1[offset1+n] ^ buffer2[offset2+n]);
			n++;
		}
	}
	public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
	public static String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xFF;
			if (v < 16) sb.append('0');
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}
	
	public static String hex2ascii(String hex){
		String str="";
		StringBuilder sb = new StringBuilder();
		for(int i=0; i< hex.length(); i+=2){
			str=hex.substring(i, i+2);
			sb.append((char)Integer.parseInt(str,16));
		}
		return sb.toString();		
	}
	public static void writeToLogs(String logFilePath, String logString){
		FileWriter f = null;
		PrintWriter pw = null;
		try	{
			f = new FileWriter(logFilePath,true); //append
			pw = new PrintWriter(f);
			pw.println(Utilities.getSystemDate()+" "+logString);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != f)
					f.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}